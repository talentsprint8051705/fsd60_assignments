<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Form</title>
</head>
<body>
<form action="RegisterServlet" method="post">
    <h1>Registration Form</h1>
    <table>
        <tr>
            <td>Enter empId</td>
            <td><input type="number" name="empId"></td>
        </tr>
        <tr>
            <td>Enter empName</td>
            <td><input type="text" name="empName"></td>
        </tr>
        <tr>
            <td>Select Gender</td>
            <td>
                <input type="radio" name="gender" id="Male" value="Male">
                <label for="Male">Male</label>
                <input type="radio" name="gender" id="Female" value="Female">
                <label for="Female">Female</label>
            </td>
        </tr>
        <tr>
            <td>Enter salary</td>
            <td><input type="number" name="salary"></td>
        </tr>
        <tr>
            <td>Enter EmailId</td>
            <td><input type="email" name="emailId"></td>
        </tr>
        <tr>
            <td>Enter Password</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td ><button type="submit">Register</button></td>
        </tr>
    </table>
</form>
</body>
</html>

====================================================================================================
package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
		
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String EmpId = request.getParameter("empId");
		String EmpName = request.getParameter("empName");
		String gender = request.getParameter("gender");
		String Salary = request.getParameter("salary");
		String EmailId = request.getParameter("emailId");
		String Password = request.getParameter("password");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		out.print("<html>");
		out.print("<body>");
		out.print("EmpId is  : " + EmpId + "<br>");
		out.print("EmpName is: " + EmpName + "<br>");
		out.print("Gender    : " + gender + "<br>");
		out.print("salary is : " + Salary + "<br>");
		out.print("EmailId is: " + EmailId + "<br>");
		out.print("password  : " + Password);
		out.print("</body>");
		out.print("</html>");
		
	}

}